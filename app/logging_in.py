from fastapi import FastAPI
from pydantic import BaseModel
from datetime import datetime
import csv
import nest_asyncio
import uvicorn
from IPython.display import display, FileLink

# Apply the nest_asyncio patch to allow running uvicorn within Jupyter
nest_asyncio.apply()

# Define the filename for your CSV file
filename = 'log_data.csv'

# Define the header for your CSV file
header = ['Date-Time', 'Facility', 'Room Number', 'netid', 'Wifi', 'Device']

# Check if the file exists, if not, create it and write the header
try:
    with open(filename, 'r') as file:
        pass
except FileNotFoundError:
    with open(filename, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(header)

# Initialize FastAPI
app = FastAPI()

# Define the data model using Pydantic
class LogData(BaseModel):
    facility: str
    room_number: str
    netid: str
    wifi: str
    device: str

@app.post("/log")
async def log_data(data: LogData):
    # Get the current date and time
    current_time = datetime.now().strftime('%m/%d/%Y %H:%M')
    
    # Data to be logged
    log_entry = [current_time, data.facility, data.room_number, data.netid, data.wifi, data.device]
    
    # Write the data to the CSV file
    with open(filename, 'a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(log_entry)
    
    return {"message": "Data logged successfully", "data": log_entry}

# Function to run the FastAPI app
def run_app():
    uvicorn.run(app, host="0.0.0.0", port=8000)

# Display a download link for the CSV file
def display_download_link():
    display(FileLink(filename))

# Run the FastAPI server
run_app()