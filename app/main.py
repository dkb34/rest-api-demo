from fastapi import FastAPI
from pydantic import BaseModel
from typing import List
from datetime import datetime, timedelta
import random

app = FastAPI()

class CO2Data(BaseModel):
    timestamp: datetime
    co2: float

@app.get("/co2-data", response_model=List[CO2Data])
def get_fake_co2_data():
    fake_data = []
    base_time = datetime.now()
    for i in range(30):
        fake_data.append(CO2Data(
            timestamp=base_time - timedelta(minutes=i),
            co2=random.uniform(300.0, 400.0)
        ))
    return fake_data

@app.get("/")
def read_root():
    return {"message": "Welcome to the FastAPI server"}
